../bin/run zprime --individual muic_bsm_zp_s_to_b \
'{"input": "../in/muic_bsm_zp_s_to_b_50k.root", "sigma": 3.3702e-12, "sigma_error": 0.985e-12, "min_q2": 1, "max_q2": -1}'
../bin/run zprime --individual muic_1TeV_bsm_zp_s_to_b \
'{"input": "../in/muic_1TeV_bsm_zp_s_to_b_50k.root", "sigma": 260.49e-12, "sigma_error": 75.4e-12, "min_q2": 1, "max_q2": -1}'
../bin/run zprime --individual muic_5TeV_bsm_zp_s_to_b \
'{"input": "../in/muic_5TeV_bsm_zp_s_to_b_50k.root", "sigma": 0.43849e-12, "sigma_error": 0.0685e-12, "min_q2": 1, "max_q2": -1}'
../bin/run zprime --individual muic_10TeV_bsm_zp_s_to_b \
'{"input": "../in/muic_10TeV_bsm_zp_s_to_b_50k.root", "sigma": 0.027567e-12, "sigma_error": 1.76e-17, "min_q2": 1, "max_q2": -1}'