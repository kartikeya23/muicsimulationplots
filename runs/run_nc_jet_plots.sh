# # sigma=Madgraph Xsec sigma_error=Madgraph Xsec Error
# ../bin/run zprime --individual muic_nc_bj \
# '{"input": "../in/muic_nc_bj_q21_ymin0p01_ymax0p9_1M.root", "sigma": 681960e-12, "sigma_error": 143e-12, "min_q2": 1, "max_q2": -1}'
# ../bin/run zprime --individual muic_nc_cj \
# '{"input": "../in/muic_nc_cj_q21_ymin0p01_ymax0p9_1M.root", "sigma": 49713000e-12, "sigma_error": 13700e-12, "min_q2": 1, "max_q2": -1}'
# ../bin/run zprime --individual muic_nc_cbj \
# '{"input": "../in/muic_nc_cbj_q21_ymin0p01_ymax0p9_1M.root", "sigma": 50412000e-12, "sigma_error": 10800e-12, "min_q2": 1, "max_q2": -1}'
../bin/run zprime --comparison muic_bsm_zp_s_to_b_mass_comparison \
'{"name": "NC-b-jet", "input": "root_files/muic_nc_bj.root"}' \
'{"name": "NC-c-jet", "input": "root_files/muic_nc_cj.root"}' \
'{"name": "NC-cb-jet", "input": "root_files/muic_nc_cbj.root"}'