../bin/run zprime --comparison muic_bsm_zp_s_to_b_mass_comparison \
'{"name": "1TeV", "input": "root_files/muic_1TeV_bsm_zp_s_to_b.root"}' \
'{"name": "3TeV", "input": "root_files/muic_bsm_zp_s_to_b.root"}' \
'{"name": "5TeV", "input": "root_files/muic_5TeV_bsm_zp_s_to_b.root"}' \
'{"name": "10TeV", "input": "root_files/muic_10TeV_bsm_zp_s_to_b.root"}' \
'{"name": "NC-cb-jet", "input": "root_files/muic_nc_bj.root"}'