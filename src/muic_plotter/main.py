import argparse

import muic_plotter.dis.subcommand as dis
import muic_plotter.zprime.subcommand as zprime
from commons.execution import runtime


def main():
    # PARSER
    parser = argparse.ArgumentParser(description='MUIC Studies')

    parser.add_argument('--debug', dest='debug_en', action='store_const',
                        const=True, default=False,
                        help='Add additional information to stdout')

    subparsers = parser.add_subparsers(dest='parser', help='Plotting commands')

    dis.configure_parser(subparsers)
    zprime.configure_parser(subparsers)

    args = parser.parse_args()

    if args.debug_en:
        print('Debug Mode Enabled!')
        runtime.debug = True

    if args.parser == 'dis':
        dis.run(args)
    if args.parser == 'zprime':
        zprime.run(args)
