import array

import numpy as np
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TLatex
from ROOT import TLegend
from ROOT import gStyle
from ROOT import kAzure
from ROOT import kBlue
from ROOT import kCyan
from ROOT import kGreen
from ROOT import kMagenta
from ROOT import kRainBow
from ROOT import kRed

from commons.analysis.plotting import AbstractPlotter
from muic_plotter.commons.plots import *


class SimulationPlotter(AbstractPlotter):

    def __init__(self):
        super().__init__()

        # STATS
        self.events_per_group = {}

        # BINS
        q2bins = []

        for p in range(9):
            for i in range(1, 10):
                q2bins.append(i * math.pow(10, p))

        xbins = []

        for p in range(6):
            for i in range(1, 10):
                xbins.append(i * math.pow(10, p - 6))

        xbins.append(1)

        self.xbins = array.array('d', xbins)

        self.q2bins = array.array('d', q2bins)

        self.q2bins_high = []

        for value in self.q2bins:
            if value > 80:
                self.q2bins_high.append(value)

        self.q2bins_high = array.array('d', self.q2bins_high)

        # TO COMBINE
        self.plot_groups = {
            'agljb_res1d': dict(),
            'acceptance_vs_Q2_prof': dict(),
            'acceptance_vs_x_prof': dict(),
            'acceptance_vs_y_prof': dict(),
        }

    def process(self, data):
        # UNPACK
        meta = data[0]
        event = data[1]

        # PLOT VARS
        Q2_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                             'Q2_1d', 'Q^{2}_{true}',
                                             'Q^{2}_{true}',
                                             xbins=np.arange(1, 1e8, 1e3),
                                             logx=True, logy=False)
        Q2_true_1d_plot.fill(event.rec_Q2, event.evt_weight)
        x_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                            'x_1d', 'x_{true}',
                                            'x_{true}',
                                            xbins=np.arange(1e-3, 1, 1e-3),
                                            logx=True, logy=False)
        x_true_1d_plot.fill(event.rec_x, event.evt_weight)
        y_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                            'y_1d', 'y_{true}',
                                            'y_{true}',
                                            xbins=np.arange(1e-2, 1, 1e-2),
                                            logx=True, logy=False)
        y_true_1d_plot.fill(event.rec_y, event.evt_weight)
        agljb_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                                'agljb_1d', '#gamma_{true}',
                                                '#gamma_{true} [rad]',
                                                nxbins=100, x_low=0, x_up=3.5,
                                                logx=False, logy=False)
        agljb_true_1d_plot.fill(event.rec_agljb, event.evt_weight)
        eta_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                              'eta_had_1d', '#eta_{had,true}',
                                              '#eta_{had,true}',
                                              nxbins=100, x_low=-10, x_up=10,
                                              logx=False, logy=False)
        eta_true_1d_plot.fill(-math.log(math.tan(event.rec_agljb / 2)), event.evt_weight)
        eta_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                              'eta_mu_1d', '#eta_{#mu,true}',
                                              '#eta_{#mu,true}',
                                              nxbins=100, x_low=-10, x_up=10,
                                              logx=False, logy=False)
        eta_true_1d_plot.fill(event.mu_eta, event.evt_weight)

        # SIGMA
        # Sigma is in mb, so multiply by 1e9 to get pb
        Q2_high_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                              'Q2_high_dcs', 'Differential Cross Section',
                                              'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
                                              xbins=self.q2bins_high,
                                              logx=True, logy=True)
        Q2_high_dcs_plot.fill(event.rec_Q2,
                              event.evt_weight,
                              meta['__id__'],
                              meta['sigma'] / meta['weight_sum'] * 1e9,
                              meta['sigma_error'] / meta['weight_sum'] * 1e9)

        Q2_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                         'Q2_dcs', 'Differential Cross Section',
                                         'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
                                         xbins=self.q2bins,
                                         logx=True, logy=True)
        Q2_dcs_plot.fill(event.rec_Q2,
                         event.evt_weight,
                         meta['__id__'],
                         meta['sigma'] / meta['weight_sum'] * 1e9,
                         meta['sigma_error'] / meta['weight_sum'] * 1e9)

        x_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                        'x_dcs', 'Differential Cross Section',
                                        'x', 'd#sigma / dx [pb]',
                                        xbins=self.xbins,
                                        logx=True, logy=True)
        x_dcs_plot.fill(event.rec_x,
                        event.evt_weight,
                        meta['__id__'],
                        meta['sigma'] / meta['weight_sum'] * 1e9,
                        meta['sigma_error'] / meta['weight_sum'] * 1e9)

        y_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                        'y_dcs', 'Differential Cross Section',
                                        'y', 'd#sigma / dy [pb]',
                                        nxbins=100, x_low=0, x_up=1,
                                        logx=False, logy=True)
        y_dcs_plot.fill(event.rec_y,
                        event.evt_weight,
                        meta['__id__'],
                        meta['sigma'] / meta['weight_sum'] * 1e9,
                        meta['sigma_error'] / meta['weight_sum'] * 1e9)

        # LEPTONS ONLY
        Q2jb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                     'Q2mu_Q2_vs_x_res2d', '#Delta Q^{2} / Q^{2} Muon Only',
                                                     'X', 'Q^{2} [GeV^{2}]',
                                                     xbins=self.xbins, ybins=self.q2bins)
        Q2jb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recsm_Q2 / event.rec_Q2 - 1, event.evt_weight)

        xjb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                    'xmu_Q2_vs_x_res2d', '#Delta x / x Muon Only',
                                                    'X', 'Q^{2} [GeV^{2}]',
                                                    xbins=self.xbins, ybins=self.q2bins)
        xjb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recsm_x / event.rec_x - 1, event.evt_weight)

        yjb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                    'ymu_Q2_vs_x_2d', '#Delta y / y Muon Only',
                                                    'X', 'Q^{2} [GeV^{2}]',
                                                    xbins=self.xbins, ybins=self.q2bins)
        yjb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recsm_y / event.rec_y - 1, event.evt_weight)

        # FIND NOCUTS RECONSTRUCTION
        nocuts_i_rec = -1

        for i_recg in range(event.vsize_recg):
            name_rec = event.recg_name[i_recg]

            if name_rec == 'nocuts':
                nocuts_i_rec = i_recg
                break

        # LOOP RECONSTRUCTION GROUPS
        for i_recg in range(event.vsize_recg):
            # SHORT-CIRCUIT
            if event.recg_had_n[i_recg] == 0:
                continue

            if nocuts_i_rec < 0:
                print('Missing nocuts')
                continue

            # GET RECONSTRUCTION NAME
            name_rec = event.recg_name[i_recg]

            # ADD EVENTS
            if name_rec in self.events_per_group:
                self.events_per_group[name_rec] += 1
            else:
                self.events_per_group[name_rec] = 1

            # PLOT ENERGY-ETA CUTS
            if name_rec not in ['nocuts', 'eta_2p4', 'eta_3', 'eta_4', 'eta_5', 'eta_gtm4p0_lt2p4', 'eta_gtm5p0_lt2p4']:
                energy_ranges = [[0, -1], [10, 40], [40, 160], [160, -1]]

                for energy_range in energy_ranges:
                    # SHORT-CIRCUIT
                    passes_low = energy_range[0] == -1 or energy_range[0] < event.recg_sum_e[i_recg]
                    passes_up = energy_range[1] == -1 or event.recg_sum_e[i_recg] < energy_range[1]

                    if not (passes_low and passes_up):
                        continue

                    # GENERATE NAME
                    energy_range_name = '_e'
                    energy_range_name += '' if energy_range[0] < 0 else '_gt%0.1f' % energy_range[0]
                    energy_range_name += '' if energy_range[1] < 0 else '_lt%0.1f' % energy_range[1]

                    energy_range_name = energy_range_name.replace('.', 'p')
                    energy_range_name = energy_range_name.replace('-', 'm')

                    # PLOT GAMMA DISTRIBUTIONS
                    agljb_vs_agljb_res1d_plot = self.checkout_plot(RMS1DPlot,
                                                                   'agljb_' + name_rec + energy_range_name + '_res1d',
                                                                   '#Delta #gamma / #gamma JB',
                                                                   '#gamma_{true} [rad]',
                                                                   '#Delta #gamma / #gamma JB',
                                                                   nxbins=35, x_low=0, x_up=3.5,
                                                                   y_low=0, y_up=1.2)
                    agljb_vs_agljb_res1d_plot.fill(event.rec_agljb, event.recg_agljb[i_recg] / event.rec_agljb - 1,
                                                   event.evt_weight)

            # PLOT ETA CUTS
            if name_rec in ['nocuts', 'eta_2p4', 'eta_3', 'eta_4', 'eta_5', 'eta_gtm4p0_lt2p4', 'eta_gtm5p0_lt2p4', ]:
                # 2D RESOLUTIONS
                Q2jb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                             'Q2jb_Q2_vs_x_' + name_rec + '_res2d',
                                                             '#Delta Q^{2} / Q^{2} JB',
                                                             'X', 'Q^{2} [GeV^{2}]',
                                                             xbins=self.xbins, ybins=self.q2bins)
                Q2jb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_Q2jb[i_recg] / event.rec_Q2 - 1,
                                             event.evt_weight)

                xjb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                            'xjb_Q2_vs_x_' + name_rec + '_res2d',
                                                            '#Delta x / x JB',
                                                            'X', 'Q^{2} [GeV^{2}]',
                                                            xbins=self.xbins, ybins=self.q2bins)
                xjb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_xjb[i_recg] / event.rec_x - 1,
                                            event.evt_weight)

                yjb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                            'yjb_Q2_vs_x_' + name_rec + '_res2d',
                                                            '#Delta y / y JB',
                                                            'X', 'Q^{2} [GeV^{2}]',
                                                            xbins=self.xbins, ybins=self.q2bins)
                yjb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_yjb[i_recg] / event.rec_y - 1,
                                            event.evt_weight)

                agljb_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                              'agljb_Q2_vs_x_' + name_rec + '_res2d',
                                                              '#Delta #gamma / #gamma JB',
                                                              'X', 'Q^{2} [GeV^{2}]',
                                                              xbins=self.xbins, ybins=self.q2bins)
                agljb_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_agljb[i_recg] / event.rec_agljb - 1,
                                              event.evt_weight)

                Q2DA_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                             'Q2DA_Q2_vs_x_' + name_rec + '_res2d',
                                                             '#Delta Q^{2} / Q^{2} DA',
                                                             'X', 'Q^{2} [GeV^{2}]',
                                                             xbins=self.xbins, ybins=self.q2bins)
                Q2DA_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_Q2DA[i_recg] / event.rec_Q2 - 1,
                                             event.evt_weight)

                xDA_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                            'xDA_Q2_vs_x_' + name_rec + '_res2d',
                                                            '#Delta x / x DA ',
                                                            'X', 'Q^{2} [GeV^{2}]',
                                                            xbins=self.xbins, ybins=self.q2bins)
                xDA_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_xDA[i_recg] / event.rec_x - 1,
                                            event.evt_weight)

                yDA_Q2_vs_x_res2d_plot = self.checkout_plot(RMS2DPlot,
                                                            'yDA_Q2_vs_x_' + name_rec + '_res2d',
                                                            '#Delta y / y DA',
                                                            'X', 'Q^{2} [GeV^{2}]',
                                                            xbins=self.xbins, ybins=self.q2bins)
                yDA_Q2_vs_x_res2d_plot.fill(event.rec_x, event.rec_Q2, event.recg_yDA[i_recg] / event.rec_y - 1,
                                            event.evt_weight)

                # 1D RESOLUTIONS
                agljb_vs_agljb_res1d_plot = self.checkout_plot(RMS1DPlot,
                                                               'agljb_' + name_rec + '_res1d',
                                                               '#Delta #gamma / #gamma JB',
                                                               '#gamma_{true} [rad]',
                                                               '#Delta #gamma / #gamma JB',
                                                               nxbins=35, x_low=0, x_up=3.5,
                                                               y_low=0, y_up=1.2)
                agljb_vs_agljb_res1d_plot.fill(event.rec_agljb, event.recg_agljb[i_recg] / event.rec_agljb - 1,
                                               event.evt_weight)

                self.plot_groups['agljb_res1d'][name_rec] = agljb_vs_agljb_res1d_plot.plot

                # HADRON DISTRIBUTIONS
                ehad_1d_plot = self.checkout_plot(Hist1DPlot,
                                                  'ehad_' + name_rec + '_1d', 'E_{had}', 'E_{had}',
                                                  nxbins=100, x_low=0, x_up=1000,
                                                  logx=True, logy=False)
                ehad_1d_plot.fill(event.recg_sum_e[i_recg], event.evt_weight)

                # ACCEPTANCE DISTRIBUTIONS
                acceptance = event.recg_had_n[i_recg] / event.recg_had_n[nocuts_i_rec]

                acceptance_vs_Q2_prof_plot = self.checkout_plot(ProfilePlot,
                                                                'acceptance_vs_Q2_' + name_rec + '_prof',
                                                                'Cut Acceptance',
                                                                'Q^{2}_{true} [GeV^{2}]', 'Acceptance',
                                                                xbins=self.q2bins,
                                                                y_low=0, y_up=1.2,
                                                                logx=True)
                acceptance_vs_Q2_prof_plot.fill(event.rec_Q2, acceptance, event.evt_weight)

                acceptance_vs_x_prof_plot = self.checkout_plot(ProfilePlot,
                                                               'acceptance_vs_x_' + name_rec + '_prof',
                                                               'Cut Acceptance',
                                                               'x_{true}', 'Acceptance',
                                                               xbins=self.xbins,
                                                               y_low=0, y_up=1.2,
                                                               logx=True)
                acceptance_vs_x_prof_plot.fill(event.rec_x, acceptance, event.evt_weight)

                acceptance_vs_y_prof_plot = self.checkout_plot(ProfilePlot,
                                                               'acceptance_vs_y_' + name_rec + '_prof',
                                                               'Cut Acceptance',
                                                               'y_{true}', 'Acceptance',
                                                               nxbins=100, x_low=0, x_up=1,
                                                               y_low=0, y_up=1.2,
                                                               logx=False)
                acceptance_vs_y_prof_plot.fill(event.rec_y, acceptance, event.evt_weight)

                acceptance_1d_plot = self.checkout_plot(Hist1DPlot,
                                                        'acceptance_' + name_rec + '_1d',
                                                        'Cut Acceptance',
                                                        'Acceptance',
                                                        nxbins=100, x_low=0, x_up=1,
                                                        logx=False, logy=False)
                acceptance_1d_plot.fill(acceptance, event.evt_weight)

                # REGISTER FOR GROUPING
                self.plot_groups['acceptance_vs_Q2_prof'][name_rec] = acceptance_vs_Q2_prof_plot.plot
                self.plot_groups['acceptance_vs_x_prof'][name_rec] = acceptance_vs_x_prof_plot.plot
                self.plot_groups['acceptance_vs_y_prof'][name_rec] = acceptance_vs_y_prof_plot.plot

    def post_production(self):
        # STYLES
        gStyle.SetTitleY(.975)
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFont(42)
        gStyle.SetLegendTextSize(0.04)
        gStyle.SetErrorX(0.0)
        gStyle.SetOptStat(0)
        gStyle.SetPalette(kRainBow)

        serie_styles = {
            'eta_2p4': {'label': '|#eta| < 2.4', 'color': kRed, 'marker': 20},
            'eta_3': {'label': '|#eta| < 3', 'color': kBlue, 'marker': 21},
            'eta_4': {'label': '|#eta| < 4', 'color': kMagenta, 'marker': 22},
            'eta_5': {'label': '|#eta| < 5', 'color': kGreen + 4, 'marker': 23},
            'eta_gtm4p0_lt2p4': {'label': '-4 < #eta < 2.4', 'color': kAzure + 3, 'marker': 34},
            'eta_gtm5p0_lt2p4': {'label': '-5 < #eta < 2.4', 'color': kCyan + 4, 'marker': 34},
        }

        # AGLJB
        agg_plot_styles = {
            'agljb_res1d': {'title': '#Delta #gamma / #gamma JB', 'x_low': 0, 'x_up': 3.5, 'y_low': 0, 'y_up': 1.2},
        }

        for plot_group_name in ['agljb_res1d']:
            plot_group = self.plot_groups[plot_group_name]
            agg_plot_style = agg_plot_styles[plot_group_name]

            plot_canvas = TCanvas(plot_group_name, '', 1080, 1080)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.cd()

            frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                          agg_plot_style['x_up'], agg_plot_style['y_up'],
                                          agg_plot_style['title'])
            frame.GetXaxis().SetTitle('#Delta #gamma / #gamma JB')
            frame.GetXaxis().SetTitleOffset(1.350)

            text_label = TLatex()
            text_label.SetTextSize(0.035)
            text_label.DrawLatexNDC(0.150, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
            text_label.Draw('SAME')

            legend_x0 = 0.150
            legend_y0 = 0.650

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.4, legend_y0 + 0.150)
            legend.SetNColumns(2)
            legend.SetMargin(0.5)
            legend.SetFillStyle(0)

            copies = list()

            for serie_name in ['eta_2p4', 'eta_3', 'eta_4', 'eta_5', 'eta_gtm4p0_lt2p4', 'eta_gtm5p0_lt2p4']:
                plot = plot_group.get(serie_name)

                if plot is None:
                    continue

                plot = plot.Clone()
                style = serie_styles[serie_name]

                plot.SetMarkerStyle(style['marker'])
                plot.SetMarkerColor(style['color'])
                plot.SetMarkerSize(2)

                legend.AddEntry(plot, style['label'], 'P')

                plot.Draw('HIST SAME P X0')

                copies.append(plot)

            legend.Draw()

            plot_canvas.SaveAs(plot_group_name + '.png')

        # ACCEPTANCE
        agg_plot_styles = {
            'acceptance_vs_Q2_prof': {'title': 'Cut Acceptance', 'x_low': min(self.q2bins), 'x_up': max(self.q2bins),
                                      'y_low': 0, 'y_up': 1.2, 'xtitle': 'Q^{2}_{true} [GeV^{2}]',
                                      'logx': True},
            'acceptance_vs_x_prof': {'title': 'Cut Acceptance', 'x_low': min(self.xbins), 'x_up': max(self.xbins),
                                     'y_low': 0, 'y_up': 1.2, 'xtitle': 'x_{true}',
                                     'logx': True},
            'acceptance_vs_y_prof': {'title': 'Cut Acceptance', 'x_low': 0, 'x_up': 1,
                                     'y_low': 0, 'y_up': 1.2, 'xtitle': 'y_{true}',
                                     'logx': False},
        }

        for plot_group_name in ['acceptance_vs_Q2_prof', 'acceptance_vs_x_prof', 'acceptance_vs_y_prof']:
            plot_group = self.plot_groups[plot_group_name]
            agg_plot_style = agg_plot_styles[plot_group_name]

            plot_canvas = TCanvas(plot_group_name, '', 1080, 1080)
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.SetLogx(1 if agg_plot_style['logx'] else 0)
            plot_canvas.cd()

            frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                          agg_plot_style['x_up'], agg_plot_style['y_up'],
                                          agg_plot_style['title'])
            frame.GetXaxis().SetTitle(agg_plot_style['xtitle'])
            frame.GetYaxis().SetTitle('Acceptance')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitleOffset(1.650)

            text_label = TLatex()
            text_label.SetTextSize(0.035)
            text_label.DrawLatexNDC(0.235, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
            text_label.Draw('SAME')

            legend_x0 = 0.200
            legend_y0 = 0.150

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.6, legend_y0 + 0.125)
            legend.SetNColumns(3)
            legend.SetMargin(0.5)
            legend.SetFillStyle(0)

            copies = list()

            for serie_name in ['eta_2p4', 'eta_3', 'eta_4', 'eta_5', 'eta_gtm4p0_lt2p4', 'eta_gtm5p0_lt2p4']:
                plot = plot_group.get(serie_name)

                if plot is None:
                    continue

                plot = plot.Clone()
                style = serie_styles[serie_name]

                plot.SetMarkerStyle(style['marker'])
                plot.SetMarkerColor(style['color'])
                plot.SetMarkerSize(2)

                legend.AddEntry(plot, style['label'], 'P')

                plot.Draw('HIST SAME P X0')

                copies.append(plot)

            legend.Draw()

            plot_canvas.SaveAs(plot_group_name + '.png')

        # PRINT STATS
        total_events = self.events_per_group['nocuts']

        for key, nevents in self.events_per_group.items():
            print('%s: events=%d event_acceptance=%f' % (key, nevents, nevents / total_events))


class SimulationComparisonPlotter(AbstractPlotter):

    def __init__(self):
        super().__init__()

        self.fitted_functions = dict()

        # PLOT GROUPS
        self.plot_group_rules = {
            # MACHINES
            'LHmuC': lambda simulation: 'LHmuC_' in simulation,
            'muic2': lambda simulation: 'muic2_' in simulation,
            'muic': lambda simulation: 'muic_' in simulation and 'muic_hera_' not in simulation,
            'muic_hera': lambda simulation: 'muic_hera_' in simulation,
            # BEAMS
            'nc_mu': lambda simulation: '_nc_mu' in simulation,
            'nc_anti_mu': lambda simulation: '_nc_anti_mu' in simulation,
            'cc_mu': lambda simulation: '_cc_mu' in simulation,
            'cc_anti_mu': lambda simulation: '_cc_anti_mu' in simulation,
        }

        # NOTE PLOT GROUPS STRUCTURE = PLOT GROUP NAME -> CLASSIFIER NAME -> SIMULATION NAME
        self.plot_groups = {
            'Q2_dcs': dict(),
            'Q2_high_dcs': dict()
        }

    def process(self, data):
        # UNPACK
        simulation = data[0]
        root_file = data[1]

        # CLASSIFY
        for plot_name, groups in self.plot_groups.items():
            for rule_name, rule in self.plot_group_rules.items():
                # SHORT-CIRCUIT
                if not rule(simulation):
                    continue

                # GET GROUP
                group = groups.get(rule_name)

                if group is None:
                    group = dict()
                    groups[rule_name] = group

                # GET PLOT
                plot = getattr(root_file, plot_name)

                if plot is not None:
                    plot = plot.Clone(rule_name + '-' + plot_name)

                # ADD TO GROUP
                group[simulation] = plot

    def init_fitted_function(self, function_variant, function_name, plot, params):
        # FIND MIN AND MAX
        nxbins = plot.GetXaxis().GetNbins()

        min_x = None
        max_x = None
        max_y = None

        for i in range(1, nxbins + 1):
            content = plot.GetBinContent(i)
            x_low = plot.GetXaxis().GetBinLowEdge(i)
            width = plot.GetXaxis().GetBinWidth(i)
            x_up = x_low + width

            if content != 0:
                if max_x is None or max_x < x_up:
                    max_x = x_up

                if min_x is None or x_low < min_x:
                    min_x = x_low

                if max_y is None or max_y < content:
                    max_y = content

        min_x = max(200, min_x)

        # CREATE FUNCTION
        function = None

        if function_variant in ['nc_mu', 'nc_anti_mu']:
            function = TF1(function_name + '_func',
                           '[2] * pow(x, [3]) * (1 + 1 / pow(1 + [1] / x, 2) + 2 / (1 + [1] / x)) * pow(1 - x / [0], [4])',
                           min_x, max_x)

            Emu = params['Emu']
            Ep = params['Ep']

            function.FixParameter(0, 4 * Emu * Ep)  # MAX Q2
            function.FixParameter(1, 91.1876 ** 2)  # MASS OF Z SQUARED
            function.SetParameter(2, 1e6)
            function.SetParLimits(2, 1e5, 1e10)
            function.SetParameter(3, -2)
            function.SetParLimits(3, -3, -2)
            function.SetParameter(4, 0)
            function.SetParLimits(4, 0, 30)

            # FIT
            res = plot.Fit(function, '0 M S Q R')

            print('%s,%f,%f,%f' % (function_name, res.Parameter(2), res.Parameter(3), res.Parameter(4)))

        elif function_variant in ['cc_mu', 'cc_anti_mu']:
            function = TF1(function_name + '_func',
                           '[2] * pow(x, [3]) * (1 / pow(1 + [1] / x + pow([5], 4)/pow(x,2), 2)) * pow(1 - x / [0], [4])',
                           min_x, max_x)

            Emu = params['Emu']
            Ep = params['Ep']

            function.FixParameter(0, 4 * Emu * Ep)  # MAX Q2
            function.FixParameter(1, 80.379 ** 2)  # MASS OF W SQUARED
            function.SetParameter(2, 1)
            function.SetParLimits(2, 1, 1e12)
            function.SetParameter(3, -2)
            function.SetParLimits(3, -3, -2)
            function.SetParameter(4, 0)
            function.SetParLimits(4, 0, 30)
            function.SetParameter(5, 0)
            function.SetParLimits(5, 0, 100)

            # FIT
            res = plot.Fit(function, '0 M S Q R')

            print('%s,%f,%f,%f,%f' % (
                function_name, res.Parameter(2), res.Parameter(3), res.Parameter(4), res.Parameter(5)))

        # SAVE
        self.fitted_functions[function_name] = function

    def post_production(self):
        # STYLES
        gStyle.SetTitleY(.975)
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFont(42)
        gStyle.SetLegendTextSize(0.04)
        gStyle.SetErrorX(0.0)
        gStyle.SetOptStat(0)
        gStyle.SetPalette(kRainBow)

        machine_order = ['LHmuC', 'muic2', 'muic', 'muic_hera']

        group_data = {
            # MACHINES
            'LHmuC': {'title': 'LHmuC', 'Emu': 1500, 'Ep': 7000},
            'muic2': {'title': 'MuIC2', 'Emu': 1000, 'Ep': 1000},
            'muic': {'title': 'MuIC', 'Emu': 960, 'Ep': 275},
            'muic_hera': {'title': 'HERA', 'Emu': 27.5, 'Ep': 920},
            # BEAMS
            'nc_mu': {'title': '#mu^{-} p NC'},
            'nc_anti_mu': {'title': '#mu^{+} p NC'},
            'cc_mu': {'title': '#mu^{-} p CC'},
            'cc_anti_mu': {'title': '#mu^{+} p CC'},
        }

        serie_styles = {
            # MACHINES
            'LHmuC': {'label': 'LHmuC', 'color': kMagenta, 'line-style': 1, 'marker': 20, 'marker-size': 2},
            'muic2': {'label': 'MuIC2', 'color': kBlue, 'line-style': 1, 'marker': 22, 'marker-size': 2},
            'muic': {'label': 'MuIC', 'color': kRed, 'line-style': 1, 'marker': 21, 'marker-size': 2},
            'muic_hera': {'label': 'HERA', 'color': kGreen + 4, 'line-style': 1, 'marker': 23, 'marker-size': 2},
            # BEAMS
            'nc_mu': {'label': '#mu^{-} p NC', 'color': kBlue, 'line-style': 1, 'marker': 22, 'marker-size': 2},
            'nc_anti_mu': {'label': '#mu^{+} p NC', 'color': kBlue - 7, 'line-style': 7, 'marker': 29,
                           'marker-size': 2},
            'cc_mu': {'label': '#mu^{-} p CC', 'color': kRed, 'line-style': 1, 'marker': 22, 'marker-size': 2},
            'cc_anti_mu': {'label': '#mu^{+} p CC', 'color': kRed - 7, 'line-style': 7, 'marker': 29, 'marker-size': 2},
        }

        def find_plot(group, simulation_name_contains):
            for simulation_name, plot in group.items():
                if simulation_name_contains in simulation_name:
                    return (simulation_name, plot)

            return (None, None)

        # FIT DCS
        for plot_group_name in ['Q2_high_dcs']:
            plot_group = self.plot_groups[plot_group_name]

            for rule_name in ['nc_mu', 'nc_anti_mu', 'cc_mu', 'cc_anti_mu']:
                group = plot_group.get(rule_name)

                if group is None:
                    continue

                for simulation_name, plot in group.items():
                    machine_name = simulation_name.replace('_' + rule_name, '')
                    machine_data = group_data[machine_name]

                    self.init_fitted_function(rule_name, simulation_name + '_Q2_high_dcs', plot, machine_data)
                    # self.init_fitted_function(rule_name, simulation_name + '_Q2_dcs', plot, machine_data)

        print(self.fitted_functions.keys())

        # PARTIAL CROSSECTIONS
        simulation_ics_bin_rules = {
            'Total': lambda Q2: True,
            'Q2>3e4': lambda Q2: Q2 > 3e4,
            'Q2>1e5': lambda Q2: Q2 > 1e5,
            'Q2>3e5': lambda Q2: Q2 > 3e5,
            'Q2>1e6': lambda Q2: Q2 > 1e6,
            'Q2>3e6': lambda Q2: Q2 > 3e6,
            'Q2>1e7': lambda Q2: Q2 > 1e7,
        }

        line_header = 'Dataset'

        for rule in simulation_ics_bin_rules.keys():
            line_header += ',' + rule + ',' + rule + ' Error'

        print(line_header)

        for rule_name in machine_order:
            plot_group = self.plot_groups['Q2_dcs'].get(rule_name)

            if plot_group is None:
                continue

            simulation_ics_bins = dict()
            simulation_ics_error_bins = dict()

            for simulation, plot in plot_group.items():
                nxbins = plot.GetXaxis().GetNbins()

                ics_bins = dict()
                ics_error_bins = dict()

                for i in range(1, nxbins + 1):
                    x_low = plot.GetXaxis().GetBinLowEdge(i)
                    width = plot.GetXaxis().GetBinWidth(i)
                    dcs = plot.GetBinContent(i)
                    dcs_error = plot.GetBinError(i)

                    pcs = dcs * width
                    pcs_error = dcs_error * width

                    for bin_rule_name, rule in simulation_ics_bin_rules.items():
                        if not rule(x_low):
                            continue

                        ics = ics_bins.get(bin_rule_name, 0)
                        ics_bins[bin_rule_name] = ics + pcs

                        ics_error = ics_error_bins.get(bin_rule_name, 0)
                        ics_error_bins[bin_rule_name] = ics_error + pcs_error ** 2

                simulation_ics_bins[simulation] = ics_bins
                simulation_ics_error_bins[simulation] = ics_error_bins

            for simulation in simulation_ics_bins.keys():
                line_record = simulation

                ics_bins = simulation_ics_bins[simulation]
                ics_error_bins = simulation_ics_error_bins[simulation]

                for rule in simulation_ics_bin_rules.keys():
                    ics = ics_bins.get(rule, 0)
                    ics_error = math.sqrt(ics_error_bins.get(rule, 0))

                    if ics == 0:
                        line_record += ','
                    else:
                        line_record += ',' + '{:e}'.format(ics) + ',' + '{:e}'.format(ics_error)

                print(line_record)

        # BEAM COMPARISON PLOTS
        agg_plot_styles = {
            'Q2_dcs': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e10},
            'Q2_high_dcs': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
        }

        for plot_group_name in ['Q2_dcs', 'Q2_high_dcs']:
            plot_group = self.plot_groups[plot_group_name]
            agg_plot_style = agg_plot_styles[plot_group_name]

            for rule_name in machine_order:
                group = plot_group.get(rule_name)
                gdata = group_data.get(rule_name)

                if group is None or gdata is None:
                    continue

                agg_plot_name = plot_group_name + '_' + rule_name

                title = gdata['title']

                plot_canvas = TCanvas(agg_plot_name, '', 1080, 1080)
                plot_canvas.SetLeftMargin(0.175)
                plot_canvas.SetBottomMargin(0.160)
                plot_canvas.SetLogx(1)
                plot_canvas.SetLogy(1)
                plot_canvas.cd()

                frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                              agg_plot_style['x_up'], agg_plot_style['y_up'],
                                              'Differential Cross Section (%s)' % title)
                frame.GetXaxis().SetTitle('Q^{2} [GeV^{2}]')
                frame.GetYaxis().SetTitle('d#sigma / d(Q^{2}) [pb/GeV^{2}]')
                frame.GetYaxis().SetTitleOffset(1.650)
                frame.GetXaxis().SetTitleOffset(1.350)

                text_label = TLatex()
                text_label.SetTextSize(0.035)
                text_label.DrawLatexNDC(0.235, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
                text_label.Draw('SAME')

                legend_x0 = 0.200
                legend_y0 = 0.200

                legend_2_x0 = 0.650
                legend_2_y0 = 0.750

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.2, legend_y0 + 0.1)
                legend.SetMargin(0.5)
                legend.SetFillStyle(0)

                legend_2 = TLegend(legend_2_x0, legend_2_y0, legend_2_x0 + 0.2, legend_2_y0 + 0.1)
                legend_2.SetMargin(0.5)
                legend_2.SetFillStyle(0)

                for serie_name in ['nc_mu', 'nc_anti_mu', 'cc_mu', 'cc_anti_mu']:
                    simulation_name, plot = find_plot(group, rule_name + '_' + serie_name)

                    if simulation_name is None:
                        continue

                    fitted_function = self.fitted_functions.get(simulation_name + '_' + plot_group_name)

                    style = serie_styles[serie_name]

                    plot.SetMarkerStyle(style['marker'])
                    plot.SetMarkerColor(style['color'])
                    plot.SetMarkerSize(style['marker-size'])

                    if fitted_function is not None:
                        fitted_function.SetLineColor(style['color'])
                        fitted_function.SetLineWidth(2)
                        fitted_function.SetLineStyle(style['line-style'])

                    if '_cc_' in simulation_name:
                        legend.AddEntry(plot, style['label'], 'P')
                    elif '_nc_' in simulation_name:
                        legend_2.AddEntry(plot, style['label'], 'P')

                    if fitted_function is not None:
                        fitted_function.Draw('SAME')

                    plot.Draw('SAME HIST P E X0')

                legend.Draw()
                legend_2.Draw()

                plot_canvas.SaveAs(agg_plot_name + '.png')

        # MACHINE COMPARISON PLOTS
        agg_plot_styles = {
            'Q2_dcs_nc_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e10},
            'Q2_dcs_nc_anti_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e10},
            'Q2_dcs_cc_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_dcs_cc_anti_mu': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_nc_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_nc_anti_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_cc_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
            'Q2_high_dcs_cc_anti_mu': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-12, 'y_up': 1e4},
        }

        for plot_group_name in ['Q2_dcs', 'Q2_high_dcs']:
            plot_group = self.plot_groups[plot_group_name]

            for rule_name in ['nc_mu', 'nc_anti_mu', 'cc_mu', 'cc_anti_mu']:
                agg_plot_style = agg_plot_styles[plot_group_name + '_' + rule_name]

                group = plot_group.get(rule_name)
                gdata = group_data.get(rule_name)

                if group is None or gdata is None:
                    continue

                agg_plot_name = plot_group_name + '_' + rule_name

                title = gdata['title']

                plot_canvas = TCanvas(agg_plot_name, '', 1080, 1080)
                plot_canvas.SetLeftMargin(0.175)
                plot_canvas.SetBottomMargin(0.160)
                plot_canvas.SetLogx(1)
                plot_canvas.SetLogy(1)
                plot_canvas.cd()

                frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                              agg_plot_style['x_up'], agg_plot_style['y_up'],
                                              'Differential Cross Section (%s)' % title)
                frame.GetXaxis().SetTitle('Q^{2} [GeV^{2}]')
                frame.GetYaxis().SetTitle('d#sigma / d(Q^{2}) [pb/GeV^{2}]')
                frame.GetYaxis().SetTitleOffset(1.650)
                frame.GetXaxis().SetTitleOffset(1.350)

                text_label = TLatex()
                text_label.SetTextSize(0.035)
                text_label.DrawLatexNDC(0.235, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
                text_label.Draw('SAME')

                legend_x0 = 0.200
                legend_y0 = 0.200

                legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.2, legend_y0 + 0.2)
                legend.SetMargin(0.5)
                legend.SetFillStyle(0)

                for serie_name in machine_order:
                    simulation_name, plot = find_plot(group, serie_name + '_' + rule_name)

                    if simulation_name is None:
                        continue

                    fitted_function = self.fitted_functions.get(simulation_name + '_' + plot_group_name)

                    style = serie_styles[serie_name]

                    plot.SetMarkerStyle(style['marker'])
                    plot.SetMarkerColor(style['color'])
                    plot.SetMarkerSize(style['marker-size'])

                    if fitted_function is not None:
                        fitted_function.SetLineColor(style['color'])
                        fitted_function.SetLineWidth(2)
                        fitted_function.SetLineStyle(style['line-style'])

                    legend.AddEntry(plot, style['label'], 'P')

                    if fitted_function is not None:
                        fitted_function.Draw('SAME')

                    plot.Draw('SAME HIST P E X0')

                legend.Draw()

                plot_canvas.SaveAs(agg_plot_name + '.png')
