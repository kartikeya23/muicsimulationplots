import math

from ROOT import TCanvas
from ROOT import TH1D
from ROOT import TH2D
from ROOT import TProfile
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kBlue
from ROOT import kFullCircle
from ROOT import kRed

from commons.analysis.plotting import AbstractPlot
from commons.execution import runtime


class DifferentialCrossSectionPlot(AbstractPlot):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 y_low=None, y_up=None,
                 logx=False, logy=False):

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        self.n_datasets = 0
        self.datasets = dict()

        self.plot = self.create_plot()

    def create_plot(self, ext=''):
        if all(v is not None for v in [self.nxbins, self.x_low, self.x_up]):
            return TH1D(self.name + ext, self.title, self.nxbins, self.x_low, self.x_up)
        elif all(v is not None for v in [self.xbins]):
            return TH1D(self.name + ext, self.title, self.nxbins, self.xbins)

    def checkout_partial_stats(self, dataset_name, xsec, xsec_error):
        dataset = self.datasets.get(dataset_name)

        if dataset is not None:
            return dataset

        self.n_datasets += 1

        dataset = {
            'xsec': xsec,
            'xsec_error': xsec_error,
            'plot': self.create_plot(ext=('_temp%d' % self.n_datasets))
        }

        self.datasets[dataset_name] = dataset

        return dataset

    def fill(self, x, w, dataset, xsec, xsec_error):
        partial_plot = self.checkout_partial_stats(dataset, xsec, xsec_error)['plot']
        partial_plot.Fill(x, w)

    def consolidate(self):
        total_entries = 0
        sigma_sum = 0
        sigma_error_sum = 0

        if runtime.debug:
            print('N, Bin Center, Bin Width, Value, Error')

        for i in range(1, self.nxbins + 1):
            sum_n = 0
            sum_numerator = 0
            sum_denominator = 0

            for dataset_name, dataset in self.datasets.items():
                n = dataset['plot'].GetBinContent(i)
                n_error = dataset['plot'].GetBinError(i)

                if n > 0:
                    x = dataset['xsec'] * n
                    sigma2 = (dataset['xsec_error'] * n) ** 2 + (dataset['xsec'] * n_error) ** 2

                    sum_n += n
                    sum_numerator += (x / sigma2)
                    sum_denominator += (1 / sigma2)

            if sum_n > 0:
                # CALCULATE VALUES
                average_value = sum_numerator / sum_denominator
                average_error = math.sqrt(1 / sum_denominator)

                # SUM
                total_entries += sum_n
                sigma_sum += average_value
                sigma_error_sum += average_error ** 2

                # NORMALIZE
                center = self.plot.GetXaxis().GetBinCenter(i)
                width = self.plot.GetXaxis().GetBinWidth(i)
                average_value = average_value / width
                average_error = average_error / width

                if runtime.debug:
                    print('%s, %f, %f, %s, %s' % ('{:e}'.format(sum_n), center, width,
                                                  '{:e}'.format(average_value),
                                                  '{:e}'.format(average_error)))

                # FILL
                self.plot.SetBinContent(i, average_value)
                self.plot.SetBinError(i, average_error)

        if runtime.debug:
            print('name: ' + self.name + ' N: ' + str(total_entries)
                  + ' Sigma: ' + '{:e}'.format(sigma_sum)
                  + ' Sigma Error: ' + '{:e}'.format(math.sqrt(sigma_error_sum)))

    def write(self, reviewer):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '', 1080, 1080)
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitleOffset(1.600)

        if all(v is not None for v in [self.y_low, self.y_up]):
            self.plot.GetYaxis().SetRangeUser(self.y_low, self.y_up)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullCircle)

        self.plot.Draw("P E1 E0 X0")

        plot_canvas.SaveAs(self.name + '.png')


class RMS2DPlot(AbstractPlot):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None):

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.counts = TH2D(name + '_tempN', title, nxbins, x_low, x_up, nybins, y_low, y_up)
            self.plot = TH2D(name, title, nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.counts = TH2D(name + '_tempN', title, nxbins, xbins, nybins, ybins)
            self.plot = TH2D(name, title, nxbins, xbins, nybins, ybins)

    def fill(self, x, y, z, w):
        self.counts.Fill(x, y, w)
        self.plot.Fill(x, y, w * z ** 2)

    def consolidate(self):
        for i in range(1, (self.nxbins * self.nybins) + 1):
            n = self.counts.GetBinContent(i)
            v = self.plot.GetBinContent(i)

            if n > 0:
                self.plot.SetBinContent(i, math.sqrt(v / n))

    def write(self, reviewer):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '', 1080, 1080)
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1)
        plot_canvas.SetLogy(1)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.45)
        self.plot.GetZaxis().SetRangeUser(0, 1)
        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')


class RMS1DPlot(AbstractPlot):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 y_low=None, y_up=None):

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins

        if all(v is not None for v in [x_low, x_up]):
            self.counts = TH1D(name + '_tempN', title, nxbins, x_low, x_up)
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.counts = TH1D(name + '_tempN', title, nxbins, xbins)
            self.plot = TH1D(name, title, nxbins, xbins)

    def fill(self, x, y, w):
        self.counts.Fill(x, w)
        self.plot.Fill(x, w * y ** 2)

    def consolidate(self):
        for i in range(1, self.nxbins + 1):
            n = self.counts.GetBinContent(i)
            v = self.plot.GetBinContent(i)

            if n > 0:
                self.plot.SetBinContent(i, math.sqrt(v / n))

    def write(self, reviewer):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '', 1080, 1080)
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitleOffset(1.650)

        if all(v is not None for v in [self.y_low, self.y_up]):
            self.plot.GetYaxis().SetRangeUser(self.y_low, self.y_up)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullCircle)

        self.plot.Draw("HIST P")

        plot_canvas.SaveAs(self.name + '.png')


class ProfilePlot(AbstractPlot):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 y_low=None, y_up=None,
                 logx=False, logy=False):

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        if all(v is not None for v in [nxbins, x_low, x_up, y_low, y_up]):
            self.plot = TProfile(name, title, nxbins, x_low, x_up, y_low, y_up)
        elif all(v is not None for v in [xbins, y_low, y_up]):
            self.plot = TProfile(name, title, nxbins, xbins, y_low, y_up)

    def fill(self, x, y, w):
        self.plot.Fill(x, y, w)

    def consolidate(self):
        pass

    def write(self, reviewer):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '', 1080, 1080)
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetYaxis().SetTitleOffset(1.650)

        if all(v is not None for v in [self.y_low, self.y_up]):
            self.plot.GetYaxis().SetRangeUser(self.y_low, self.y_up)

        self.plot.SetLineColor(kRed)
        self.plot.SetMarkerColor(kBlack)
        self.plot.SetMarkerStyle(kFullCircle)

        self.plot.Draw('P E')

        plot_canvas.SaveAs(self.name + '.png')


class Hist2DPlot(AbstractPlot):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None,
                 logx=False, logy=False, logz=False):

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins
        self.logx = logx
        self.logy = logy
        self.logz = logz

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.plot = TH2D(name, title, nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.plot = TH2D(name, title, nxbins, xbins, nybins, ybins)

    def fill(self, x, y, z, w):
        self.plot.Fill(x, y, z, w)

    def consolidate(self):
        pass

    def write(self, reviewer):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '', 1080, 1080)
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.SetLogz(1 if self.logz else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.45)
        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')


class Hist1DPlot(AbstractPlot):

    def __init__(self, name, title, x_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 logx=False, logy=False):

        if xbins is not None:
            nxbins = len(xbins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy

        if all(v is not None for v in [x_low, x_up]):
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.plot = TH1D(name, title, nxbins, xbins)

    def fill(self, value, w):
        self.plot.Fill(value, w)

    def consolidate(self):
        pass

    def write(self, reviewer):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(1111)

        plot_canvas = TCanvas(self.name, '', 1080, 1080)
        plot_canvas.SetTopMargin(0.110)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.cd(0)

        self.plot.SetLineColor(kBlue)
        self.plot.SetFillColor(kBlue - 7)
        self.plot.GetXaxis().SetTitleOffset(1.350)
        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.Draw('HIST')

        plot_canvas.SaveAs(self.name + '.png')
