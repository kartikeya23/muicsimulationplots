import json
import os

from ROOT import TFile
from ROOT import TH1
from ROOT import gROOT
from ROOT import gStyle
from ROOT import kFALSE
from ROOT import kTRUE

from commons.analysis.cms_style import apply_tdr_style
from muic_plotter.zprime.plotters import SimulationComparisonPlotter
from muic_plotter.zprime.plotters import SimulationPlotter


def configure_parser(subparsers):
    parser = subparsers.add_parser('zprime', help='Z\' Simulation')

    parser.add_argument('--individual', dest='individual_en', action='store_const',
                        const=True, default=False,
                        help='Generates a set of plots for each simulation given')

    parser.add_argument('--comparison', dest='comparison_en', action='store_const',
                        const=True, default=False,
                        help='Generates a set of comparison plots for the simulations given')

    parser.add_argument('study', metavar='STUDY',
                        default='study', type=str,
                        help='Name of the Study')

    parser.add_argument('datasets', metavar='DATASETS',
                        default=[], type=str, nargs='+',
                        help='List of datasets')


def run(args):
    # STUPID ROOT STUFF
    gROOT.SetBatch(kTRUE)
    TH1.AddDirectory(kFALSE)

    # STUPID STYLE STUFF
    apply_tdr_style()
    gStyle.SetOptTitle(1)
    gStyle.SetPadTopMargin(0.100)
    gStyle.SetPadBottomMargin(0.100)

    if len(args.datasets) == 0:
        print('No datasets given. Aborting')
    elif args.individual_en:
        datasets = [json.loads(dataset) for dataset in args.datasets]
        study = args.study

        # OBJECT TO TUPLE
        ndatasets = []

        for dataset in datasets:
            ndatasets.append((os.path.join('..', dataset['input']),
                              dataset['sigma'], dataset['sigma_error'],
                              dataset['min_q2'], dataset['max_q2']))

        # PROCESS ALL SIMULATIONS
        os.makedirs('root_files', exist_ok=True)
        os.makedirs(study, exist_ok=True)
        os.chdir(study)

        plot_simulation('../root_files', study, ndatasets)

        os.chdir('..')
    elif args.comparison_en:
        datasets = [json.loads(dataset) for dataset in args.datasets]

        # OBJECT TO TUPLE
        ndatasets = []

        for dataset in datasets:
            ndatasets.append((dataset['name'], dataset['input']))

        plot_simulation_comparison(ndatasets)


def plot_simulation(out_dir, study, datasets, event_limit=-1, use_upper_energy_cut=True):
    plotter = SimulationPlotter()

    # LOOP DATASETS
    dataset_id = 0

    event_limit_reached = False
    events_processed = 0

    for dataset in datasets:
        # UNPACK DATASET
        dataset_input = dataset[0]
        dataset_mg5_sigma = dataset[1]
        dataset_mg5_sigma_error = dataset[2]
        dataset_low_cutoff = dataset[3]
        dataset_up_cutoff = dataset[4]

        # LOG
        print('Reading: %s' % dataset_input)

        # OPEN
        root_file = TFile.Open(dataset_input, 'READ')

        # READ METADATA
        meta = {
            '__id__': dataset_id,
            'events_total': 0,
            'events_processed': 0,
            'events_total_weighted': 0,
            'events_processed_weighted': 0,
            'processes': list(),
            'sigma': -1,
            'sigma_error': -1,
            'processed_sigma': -1,
            'processed_sigma_error': -1,
            'weight_sum': -1
        }

        for entry in root_file.meta:
            # GET SIM DATA
            meta['events_total'] = entry.events_total
            meta['events_processed'] = entry.events_processed
            meta['events_total_weighted'] = entry.events_total_weighted
            meta['events_processed_weighted'] = entry.events_processed_weighted

            # GET SIGMA
            sigma = entry.sigma
            sigma_error = entry.sigma_error

            if dataset_mg5_sigma is not None and dataset_mg5_sigma_error is not None:
                sigma_scale = entry.sigma / dataset_mg5_sigma
                sigma_error = dataset_mg5_sigma_error * sigma_scale

            meta['sigma'] = sigma
            meta['sigma_error'] = sigma_error
            meta['processed_sigma'] = sigma * entry.events_processed_weighted / entry.weight_sum
            meta['processed_sigma_error'] = sigma_error * entry.events_processed_weighted / entry.weight_sum

            # GET WEIGHT SUM
            meta['weight_sum'] = entry.weight_sum

            # GET PROCESSES
            vsize_proc = entry.vsize_proc

            for i_proc in range(vsize_proc):
                meta['processes'].append({
                    'name': entry.proc[i_proc],
                    'sigma': entry.proc_sigma[i_proc],
                    'sigma_error': entry.proc_sigma_error[i_proc],
                    'processed_sigma': entry.proc_sigma[i_proc] * entry.events_processed_weighted / entry.weight_sum,
                    'processed_sigma_error': entry.proc_sigma_error[
                                                 i_proc] * entry.events_processed_weighted / entry.weight_sum
                })

        # SHOW META
        print(meta)
        print('Sigma (pythia): ' + '{:e}'.format(meta['sigma'] * 1e9))  # mb to pb
        print('Sigma Error (pythia): ' + '{:e}'.format(meta['sigma_error'] * 1e9))  # mb to pb
        print('Sigma (processed): ' + '{:e}'.format(meta['processed_sigma'] * 1e9))  # mb to pb
        print('Sigma Error (processed): ' + '{:e}'.format(meta['processed_sigma_error'] * 1e9))  # mb to pb

        # PROCESS EVENTS
        for event in root_file.evt:
            # SHORT-CIRCUIT: MUST PASS LOW ENERGY CUTOFF
            if dataset_low_cutoff > -1 and event.rec_Q2 < dataset_low_cutoff:
                continue

            # SHORT-CIRCUIT: MUST PASS UP ENERGY CUTOFF
            if use_upper_energy_cut and -1 < dataset_up_cutoff < event.rec_Q2:
                continue

            # PROCESS
            plotter.process((meta, event))

            # UPDATE COUNTER
            events_processed += 1

            if -1 < event_limit <= events_processed:
                event_limit_reached = True
                break

        print('root file: %s, events: %d' % (dataset_input, events_processed))

        # CLOSE
        root_file.Close()

        # SHORT-CIRCUIT
        if event_limit_reached:
            break

    # WRITE OUTPUTS
    output_root_file = TFile.Open(os.path.join(out_dir, study + '.root'), 'RECREATE')
    plotter.write()
    output_root_file.Close()


def plot_simulation_comparison(datasets):
    plotter = SimulationComparisonPlotter()

    for dataset in datasets:
        # UNPACK DATASET
        name = dataset[0]
        root_file_path = dataset[1]

        # LOG
        print('Reading: %s' % root_file_path)

        # OPEN
        root_file = TFile.Open(root_file_path, 'READ')

        # PROCESS
        plotter.process((name, root_file))

        # CLOSE
        root_file.Close()

    plotter.write()
