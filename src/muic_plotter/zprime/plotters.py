import array
from functools import reduce

import numpy as np
from ROOT import TCanvas
from ROOT import TLatex
from ROOT import TLegend
from ROOT import gStyle
from ROOT import kBlue
from ROOT import kGreen
from ROOT import kMagenta
from ROOT import kRainBow
from ROOT import kRed
from ROOT import kViolet

from commons.analysis.plotting import AbstractPlotter
from muic_plotter.commons.plots import *


class SimulationPlotter(AbstractPlotter):

    def __init__(self):
        super().__init__()

        # STATS
        self.events_per_group = {}

        # BINS
        q2bins = []

        for p in range(9):
            for i in range(1, 10):
                q2bins.append(i * math.pow(10, p))

        xbins = []

        for p in range(6):
            for i in range(1, 10):
                xbins.append(i * math.pow(10, p - 6))

        xbins.append(1)

        self.xbins = array.array('d', xbins)

        self.q2bins = array.array('d', q2bins)

        self.q2bins_high = []

        for value in self.q2bins:
            if value > 80:
                self.q2bins_high.append(value)

        self.q2bins_high = array.array('d', self.q2bins_high)

    def process(self, data):
        # UNPACK
        meta = data[0]
        event = data[1]

        # PLOT VARS
        Q2_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                             'Q2_1d', 'Q^{2}_{true}',
                                             'Q^{2}_{true}',
                                             xbins=np.arange(1, 1e8, 1e3),
                                             logx=True, logy=False)
        Q2_true_1d_plot.fill(event.rec_Q2, event.evt_weight)
        x_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                            'x_1d', 'x_{true}',
                                            'x_{true}',
                                            xbins=np.arange(1e-3, 1, 1e-3),
                                            logx=True, logy=False)
        x_true_1d_plot.fill(event.rec_x, event.evt_weight)
        y_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                            'y_1d', 'y_{true}',
                                            'y_{true}',
                                            xbins=np.arange(1e-2, 1, 1e-2),
                                            logx=True, logy=False)
        y_true_1d_plot.fill(event.rec_y, event.evt_weight)
        agljb_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                                'agljb_1d', '#gamma_{true}',
                                                '#gamma_{true} [rad]',
                                                nxbins=100, x_low=0, x_up=3.5,
                                                logx=False, logy=False)
        agljb_true_1d_plot.fill(event.rec_agljb, event.evt_weight)
        eta_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                              'eta_had_1d', '#eta_{had,true}',
                                              '#eta_{had,true}',
                                              nxbins=100, x_low=-10, x_up=10,
                                              logx=False, logy=False)
        eta_true_1d_plot.fill(-math.log(math.tan(event.rec_agljb / 2)), event.evt_weight)
        eta_true_1d_plot = self.checkout_plot(Hist1DPlot,
                                              'eta_mu_1d', '#eta_{#mu,true}',
                                              '#eta_{#mu,true}',
                                              nxbins=100, x_low=-10, x_up=10,
                                              logx=False, logy=False)
        eta_true_1d_plot.fill(event.mu_eta, event.evt_weight)

        # SIGMA
        # Sigma is in mb, so multiply by 1e9 to get pb
        Q2_high_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                              'Q2_high_dcs', 'Differential Cross Section',
                                              'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
                                              xbins=self.q2bins_high,
                                              logx=True, logy=True)
        Q2_high_dcs_plot.fill(event.rec_Q2,
                              event.evt_weight,
                              meta['__id__'],
                              meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
                              meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        Q2_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                         'Q2_dcs', 'Differential Cross Section',
                                         'Q^{2} [GeV^{2}]', 'd#sigma / d(Q^{2}) [pb/GeV^{2}]',
                                         xbins=self.q2bins,
                                         logx=True, logy=True)
        Q2_dcs_plot.fill(event.rec_Q2,
                         event.evt_weight,
                         meta['__id__'],
                         meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
                         meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        x_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                        'x_dcs', 'Differential Cross Section',
                                        'x', 'd#sigma / dx [pb]',
                                        xbins=self.xbins,
                                        logx=True, logy=True)
        x_dcs_plot.fill(event.rec_x,
                        event.evt_weight,
                        meta['__id__'],
                        meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
                        meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

        y_dcs_plot = self.checkout_plot(DifferentialCrossSectionPlot,
                                        'y_dcs', 'Differential Cross Section',
                                        'y', 'd#sigma / dy [pb]',
                                        nxbins=100, x_low=0, x_up=1,
                                        logx=False, logy=True)
        y_dcs_plot.fill(event.rec_y,
                        event.evt_weight,
                        meta['__id__'],
                        meta['sigma'] / meta['weight_sum'] * 1e9,  # mb to pb
                        meta['sigma_error'] / meta['weight_sum'] * 1e9)  # mb to pb

    def post_production(self):
        pass


class SimulationComparisonPlotter(AbstractPlotter):

    def __init__(self):
        super().__init__()

        self.agg_plot_members = {
            'Q2_dcs': dict(),
            'Q2_high_dcs': dict()
        }

        self.agg_plot_styles = {
            'Q2_dcs': {'x_low': 1, 'x_up': 5e7, 'y_low': 1e-15, 'y_up': 1e-2},
            'Q2_high_dcs': {'x_low': 200, 'x_up': 5e7, 'y_low': 1e-15, 'y_up': 1e-2},
        }

        self.serie_styles = {
            # MASSES
            'NC-b-jet': {'label': 'NC b-jet', 'color': kRed, 'line-style': 1, 'marker': 33, 'marker-size': 2},
            'NC-c-jet': {'label': 'NC b-jet', 'color': kBlue, 'line-style': 1, 'marker': 33, 'marker-size': 2},
            'NC-cb-jet': {'label': 'NC c-jet + b-jet', 'color': kViolet + 4, 'line-style': 1, 'marker': 34,
                          'marker-size': 2},

            # MASSES
            '1TeV': {'label': '1 TeV', 'color': kMagenta, 'line-style': 1, 'marker': 20, 'marker-size': 2},
            '3TeV': {'label': '3 TeV', 'color': kRed, 'line-style': 1, 'marker': 22, 'marker-size': 2},
            '5TeV': {'label': '5 TeV', 'color': kBlue, 'line-style': 1, 'marker': 21, 'marker-size': 2},
            '10TeV': {'label': '10 TeV', 'color': kGreen + 4, 'line-style': 1, 'marker': 23, 'marker-size': 2},
        }

    def process(self, data):
        # UNPACK
        name = data[0]
        root_file = data[1]

        # CLASSIFY
        for plot_name, members in self.agg_plot_members.items():
            # GET PLOT
            plot = getattr(root_file, plot_name)

            if plot is not None:
                plot = plot.Clone(name + '-' + plot_name)

            # ADD TO GROUP
            members[name] = plot

    def post_production(self):
        # STYLES
        gStyle.SetTitleY(.975)
        gStyle.SetLegendBorderSize(0)
        gStyle.SetLegendFont(42)
        gStyle.SetLegendTextSize(0.04)
        gStyle.SetErrorX(0.0)
        gStyle.SetOptStat(0)
        gStyle.SetPalette(kRainBow)

        for agg_plot_name in ['Q2_dcs', 'Q2_high_dcs']:
            agg_plot_members = self.agg_plot_members[agg_plot_name]
            agg_plot_style = self.agg_plot_styles[agg_plot_name]

            plot_canvas = TCanvas(agg_plot_name, '', 1080, 1080)
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.160)
            plot_canvas.SetLogx(1)
            plot_canvas.SetLogy(1)
            plot_canvas.cd()

            frame = plot_canvas.DrawFrame(agg_plot_style['x_low'], agg_plot_style['y_low'],
                                          agg_plot_style['x_up'], agg_plot_style['y_up'],
                                          'Differential Cross Section (m_{Z\'} Mass)')
            frame.GetXaxis().SetTitle('Q^{2} [GeV^{2}]')
            frame.GetYaxis().SetTitle('d#sigma / d(Q^{2}) [pb/GeV^{2}]')
            frame.GetYaxis().SetTitleOffset(1.650)
            frame.GetXaxis().SetTitleOffset(1.350)

            text_label = TLatex()
            text_label.SetTextSize(0.035)
            text_label.DrawLatexNDC(0.235, 0.800, '#scale[1.4]{MuIC #bf{#it{Prospective}}}')
            text_label.Draw('SAME')

            legend_x0 = 0.200
            legend_y0 = 0.200

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.2, legend_y0 + 0.2)
            legend.SetMargin(0.5)
            legend.SetFillStyle(0)

            for dataset_name, plot in agg_plot_members.items():
                style = self.serie_styles.get(dataset_name)

                if style is None:
                    continue

                plot.SetMarkerStyle(style['marker'])
                plot.SetMarkerColor(style['color'])
                plot.SetMarkerSize(style['marker-size'])

                legend.AddEntry(plot, style['label'], 'P')

                plot.Draw('SAME HIST P E X0')

            legend.Draw()

            plot_canvas.SaveAs(agg_plot_name + '.png')
