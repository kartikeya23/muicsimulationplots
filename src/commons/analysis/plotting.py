from abc import ABC
from abc import abstractmethod


class AbstractPlotter(ABC):

    def __init__(self):
        self.plots = dict()
        self.consolidated = False

    def checkout_plot(self, clazz, name, *args, **kwargs):
        plot = self.plots.get(name)

        if plot is not None:
            return plot

        plot = clazz(name, *args, **kwargs)
        self.plots[name] = plot

        return plot

    @abstractmethod
    def process(self, data):
        pass

    def consolidate(self):
        if self.consolidated:
            return

        for plot in self.plots.values():
            plot.consolidate()

        self.post_production()

        self.consolidated = True

    @abstractmethod
    def post_production(self):
        pass

    def review_plot(self, name, scope_list, **kwargs):
        pass

    def write(self):
        self.consolidate()

        for plot_name, plot in self.plots.items():
            scope_list = list()

            plot.write(lambda **kwargs: self.review_plot(plot_name, scope_list, **kwargs))


class AbstractPlot(ABC):

    @abstractmethod
    def consolidate(self):
        pass

    @abstractmethod
    def write(self, reviewer):
        pass
